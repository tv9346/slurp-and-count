import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Slurp extends HttpServlet implements Servlet {
	private minicache<String,Double> cache;

	public void init(ServletConfig config)
	throws ServletException {
		super.init(config);
		cache = new minicache<String,Double>(50);
	}

	public void destroy() {
		cache = null;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		int i = 0;
		int wc = 0;
		int cc = 0;
		double result = 0.0;
		PrintWriter out = response.getWriter();
		String url = request.getParameter("url");
		Double cachedResult = cache.get(url);
		if (cachedResult != null) {
			out.format("{\"url\": \"%s\", \"mean_length\": %.3f}", url, cachedResult.doubleValue());
			out.close();
			return;
		}

		LaxRedirectStrategy redirectStrategy = new LaxRedirectStrategy();
		CloseableHttpClient httpclient = HttpClients.custom().setRedirectStrategy(redirectStrategy).build();
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse httpResponse = httpclient.execute(httpGet);

		try {
			HttpEntity entity = httpResponse.getEntity();
			InputStream is = entity.getContent();
			int x = is.read();
			i = 0;
			while (x != -1) {
				if ((x >= 'a' && x <= 'z') ||
						(x >= 'A' && x <= 'Z') ||
						(x >= '0' && x <= '9')) {
					cc++;
					i = 1;
				} else {
					if (i == 1) {
						wc++;
					}
					i = 0;
				}
				x = is.read();
			}
			is.close();
		} finally {
			httpResponse.close();
		}
		if (wc > 0) {
			result = cc / (double) wc;
			cache.put(url, new Double(result));
			out.format("{\"url\": \"%s\", \"mean_length\": %.3f}", url, result);
		} else {
			out.format("{\"url\": \"%s\", \"state\": \"it failed\"}", url);
		}
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	{
		processRequest(request, response);
	}
}
