
  The slurp-and-count is a web application service that retrieves
an URL and calculates the average length of the words in its body.
Words are defined to be ASCII characters and numbers, all other
bytes are considered whitespace.

  To build the project, one needs Ant, Apache Commons HTTPClient
and Tomcat 7 libraries. Development happens on Java 7, other
versions are not supported (but may work). Please set the
environment variable CLASSPATH to point to a directory where these
and Java Servlet libraries are found.

  The choice of HTTP library was driven mainly by this:
http://blogs.atlassian.com/2013/07/http-client-performance-io/
Earlier and newer versions of the same libraries might work, but
are untested. The specific versions and their dependencies are
available from:

http://central.maven.org/maven2/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar
http://central.maven.org/maven2/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar
http://www-eu.apache.org/dist//commons/logging/binaries/commons-logging-1.2-bin.tar.gz


  The following assumptions and shortcuts have been:

 * Support for HTTP and HTTPS protocols is sufficient
 * If an HTTP/HTTPS resource redirects, we want to follow
 * Resources that require cookies, authentication or
   methods other than GET are not supported
